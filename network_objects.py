# coding=utf8

"""
Copyright (C) 2016 Laurent Courty

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""

from __future__ import division
import numbers
from collections import namedtuple
from inp_kwd import *

# all
NetworkElement = namedtuple('NetworkElement', ['name', 'type'])

# Nodes
SwmmNode = namedtuple('Node', ['coordinates', 'type', 'values'])

# create fields from column name (without data type and cat)
junction_fields = [i[0] for i in COLS_JUNCTION[1:]]
Junction = namedtuple('Junction', junction_fields)
outfall_fields = [i[0] for i in COLS_OUTFALL[1:]]
Outfall = namedtuple('Outfall', outfall_fields)
divider_fields = [i[0] for i in COLS_DIVIDER[1:]]
Divider = namedtuple('Divider', divider_fields)
storage_fields = [i[0] for i in COLS_STORAGE[1:]]
Storage = namedtuple('Storage', storage_fields)

# Links
SwmmLink = namedtuple('Link', ['type', 'in_node', 'out_node', 'vertices', 'values'])
XSection = namedtuple('XSection', [i[0] for i in XSECTION])
Conduit = namedtuple('Conduit', [i[0] for i in COLS_CONDUIT[1:]])
Pump = namedtuple('Pump', [i[0] for i in COLS_PUMP[1:]])
Orifice = namedtuple('Orifice', [i[0] for i in COLS_ORIFICE[1:]])
Weir = namedtuple('Pump', [i[0] for i in COLS_WEIR[1:]])
Outlet = namedtuple('Orifice', [i[0] for i in COLS_OUTLET[1:]])
