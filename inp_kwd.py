"""Defining SWMM INP keywords and specific db table columns
"""

from collections import namedtuple

# definition of the keywords of sections of inp file
KWD_SECT_TITLE = "title"                 #project title
KWD_SECT_OPTION = "option"               #analysis options
KWD_SECT_REPORT = "report"               #output reporting instructions
KWD_SECT_FILES = "files"                 #interface file options
KWD_SECT_RAINGAGE = "raingage"           #rain gage information
KWD_SECT_EVAPORATION = "evaporation"     #evaporation data
KWD_SECT_TEMPERATURE = "temperature"     #air temperature and snow melt data
KWD_SECT_SUBCATCHMENT = "subcatchment"   #basic subcatchment information
KWD_SECT_SUBAREA = "subarea"             #subcatchment impervious/pervious sub-area data
KWD_SECT_INFILTRATION = "infiltration"   #subcatchment infiltration parameters
KWD_SECT_AQUIFER = "aquifer"             #groundwater aquifer parameters
KWD_SECT_GROUNDWATER = "groundwater"     #subcatchment groundwater parameters
KWD_SECT_SNOWPACK = "snowpack"           #subcatchment snow pack parameters
KWD_SECT_JUNCTION = "junction"           #junction node information
KWD_SECT_OUTFALL = "outfall"             #outfall node information
KWD_SECT_DIVIDER = "divider"             #flow divider node information
KWD_SECT_STORAGE = "storage"             #storage node information
KWD_SECT_CONDUIT = "conduit"             #conduit link information
KWD_SECT_PUMP = "pump"                   #pump link
KWD_SECT_ORIFICE = "orifice"             #orifice link
KWD_SECT_WEIR = "weir"                   # weir link
KWD_SECT_OUTLET = "outlet"               #outlet link
KWD_SECT_XSECTION = "xsection"           #conduit, orifice, and weir cross-section geometry
KWD_SECT_LOSSE = "losse"                 #conduit entrance/exit losses and flap valve
KWD_SECT_TRANSECT = "transect"       #transect geometry for conduits with irregular cross-sections
KWD_SECT_CONTROL = "control"         #rules that control pump and regulator operation
KWD_SECT_POLLUTANT = "pollutant"     #identifies the pollutants being analyzed
KWD_SECT_LANDUSE = "landuse"         #land use categories
KWD_SECT_COVERAGE = "coverage"       #assignment of land uses to subcatchments
KWD_SECT_BUILDUP = "buildup"         #buildup functions for pollutants and land uses
KWD_SECT_WASHOFF = "washoff"         #buildup functions for pollutants and land uses
KWD_SECT_TREATMENT = "treatment"     #pollutant removal functions at conveyance system nodes
KWD_SECT_DWF = "dwf"                 #baseline dry weather sanitary inflow at nodes
KWD_SECT_PATTERN = "pattern"         #periodic variation in dry weather inflow
KWD_SECT_INFLOW = "inflow"           #external hydrograph/pollutograph inflow at nodes
KWD_SECT_LOADING = "loading"         #initial pollutant loads on subcatchments
KWD_SECT_RDII = "rdii"               #rainfall-dependent i/i information at nodes
KWD_SECT_HYDROGRAPH = "hydrograph"   #unit hydrograph data used to construct rdii inflows
KWD_SECT_CURVE = "curve"             #x-y tabular data referenced in other sections
KWD_SECT_TIMESERIE = "timeserie"     #describes how a quantity varies over time
KWD_SECT_LID_CONTROL = "lid_control" #low impact development control information
KWD_SECT_LID_USAGE = "lid_usage"     #assignment of lid controls to subcatchments
KWD_SECT_TAG = 'tag'				 # ?
KWD_SECT_MAP = 'map'				 # provides dimensions and distance units for the map
KWD_SECT_COORDINATE = 'coordinate'	 # coordinates of drainage system nodes
KWD_SECT_VERTICE = 'vertice'		 # coordinates of interior vertex points of curved drainage system links
KWD_SECT_POLYGON = 'polygon'		 # coordinates of to vertex points of polygons that define a subcatchment boundary
KWD_SECT_LABEL = 'label'			 # coordinates of user-defined map labels
KWD_SECT_BACKDROP = 'backdrop'		 # coordinates of the bounding rectangle and file name of the backdrop
KWD_SECT_SYMBOL = 'symbol'			 # coordinates of rain gage symbols
KWD_SECT_PROFILE = 'profile'		 # ?
KWD_SECT_GWF = 'gwf'				 # ?
KWD_SECT_ADJUST = 'adjustment'		 # ?



# define list of all sections keywords

KWD_SECTS = [KWD_SECT_TITLE,
             KWD_SECT_OPTION,
             KWD_SECT_REPORT,
             KWD_SECT_FILES,
             KWD_SECT_RAINGAGE,
             KWD_SECT_EVAPORATION,
             KWD_SECT_TEMPERATURE,
             KWD_SECT_SUBCATCHMENT,
             KWD_SECT_SUBAREA,
             KWD_SECT_INFILTRATION,
             KWD_SECT_AQUIFER,
             KWD_SECT_GROUNDWATER,
             KWD_SECT_SNOWPACK,
             KWD_SECT_JUNCTION,
             KWD_SECT_OUTFALL,
             KWD_SECT_DIVIDER,
             KWD_SECT_STORAGE,
             KWD_SECT_CONDUIT,
             KWD_SECT_PUMP,
             KWD_SECT_ORIFICE,
             KWD_SECT_WEIR,
             KWD_SECT_OUTLET,
             KWD_SECT_XSECTION,
             KWD_SECT_LOSSE,
             KWD_SECT_TRANSECT,
             KWD_SECT_CONTROL,
             KWD_SECT_POLLUTANT,
             KWD_SECT_LANDUSE,
             KWD_SECT_COVERAGE,
             KWD_SECT_BUILDUP,
             KWD_SECT_WASHOFF,
             KWD_SECT_TREATMENT,
             KWD_SECT_DWF,
             KWD_SECT_PATTERN,
             KWD_SECT_INFLOW,
             KWD_SECT_LOADING,
             KWD_SECT_RDII,
             KWD_SECT_HYDROGRAPH,
             KWD_SECT_CURVE,
             KWD_SECT_TIMESERIE,
             KWD_SECT_LID_CONTROL,
             KWD_SECT_LID_USAGE,
             KWD_SECT_TAG,
             KWD_SECT_MAP,
             KWD_SECT_COORDINATE,
             KWD_SECT_VERTICE,
             KWD_SECT_POLYGON,
             KWD_SECT_LABEL,
             KWD_SECT_BACKDROP,
             KWD_SECT_SYMBOL,
             KWD_SECT_PROFILE,
             KWD_SECT_GWF,
             KWD_SECT_ADJUST]


# define DB columns for each section
#~ COLS_TITLE = 		[(u'title',			'TEXT')]

#~ COLS_OPTION = 		[(u'cat',			'INTEGER PRIMARY KEY'),
					#~ (u'option_name',	'TEXT'),
					#~ (u'value',			'TEXT')]
#~ COLS_REPORT =		[(u'cat',			'INTEGER PRIMARY KEY'),
					#~ (u'option_name',	'TEXT'),
					#~ (u'value',			'TEXT')]
#~ COLS_FILES =		[(u'cat',			'INTEGER PRIMARY KEY'),
					#~ (u'option_name',	'TEXT'),
					#~ (u'value',			'TEXT')]
#~ COLS_RAINGAGE =		[(u'cat',			'INTEGER PRIMARY KEY'),
					#~ (u'name',			'TEXT'),
					#~ (u'intv1',			'TEXT'),
					#~ (u'SCF',			'REAL'),
					#~ # TIMESERIES or FILE
					#~ (u'type',			'TEXT'),
					#~ # below is mixted between types
					#~ (u'time_series',	'TEXT'),
					#~ (u'file_name',		'TEXT'),
					#~ (u'station',		'TEXT'),
					#~ (u'units',			'TEXT')]
#~ COLS_EVAPORATION =	[]
#~ COLS_TEMPERATURE =	[]
#~ COLS_SUBCATCH =		[(u'cat',			'INTEGER PRIMARY KEY'),
					#~ (u'name',			'TEXT'),
					#~ (u'rain_gage',		'TEXT'),
					#~ (u'outlet_node',	'TEXT'),
					#~ (u'area',			'REAL'),
					#~ (u'percent_imperv',	'REAL'),
					#~ (u'width',			'REAL'),
					#~ (u'slope',			'REAL'),
					#~ (u'curb_length',	'REAL'),
					#~ (u'snow_pack',		'TEXT'), 
                    #~ #begining of subarea
                    #~ (u'n_impervious',	'REAL'),
					#~ (u'n_pervious',		'REAL'),
					#~ (u'dep_storage_imperv',	'REAL'),
					#~ (u'dep_storage_perv',	'REAL'),
					#~ (u'perc_zero_stor',	'REAL'),
					#~ (u'route_to',		'TEXT'),
					#~ (u'percent_routed',	'REAL')]
#~ COLS_SUBAREA = None # included in cols_subcatch
#~ COLS_INFILTR =	[]
#~ COLS_AQUIFER =	[]
#~ COLS_GROUNDWAT =[]
#~ COLS_SNOWPACK =	[]

# Shape Geom1 Geom2 Geom3 Geom4 Barrels Culvert
# if Shape is CUSTOM or IRREGULAR :
# CUSTOM Geom1 Curve Barrels
# IRREGULAR Tsect
XSECTION = 	[(u'shape',			'TEXT'),
             (u'geom1',			'REAL'),  # Tsect in case of IRREGULAR
             (u'geom2',			'REAL'),  # curve in case of CUSTOM
             (u'geom3',			'REAL'),
             (u'geom4',			'REAL'),
             (u'barrels',		'INT'),
             (u'culvert',		'INT')]

COLS_JUNCTION = [(u'cat',           'INTEGER PRIMARY KEY'),
                 (u'name',          'TEXT'),
                 (u'invert_elev',   'REAL'),
                 (u'max_depth',     'REAL'),
                 (u'init_depth',    'REAL'),
                 (u'surcharge_depth','REAL'),
                 (u'ponded_area',   'REAL')]

COLS_OUTFALL =	[(u'cat',	'INTEGER PRIMARY KEY'),
                 (u'name',	'TEXT'),
                 (u'elev',	'REAL'),
				 # FREE, NORMAL, FIXED, TIDAL, TIMESERIES
				 (u'type',	'TEXT'),
				 # below is mixted between types
				 (u'value1',	'TEXT'),
				 (u'value2',	'TEXT')]
COLS_DIVIDER =	[(u'cat',			'INTEGER PRIMARY KEY'),
				(u'name',			'TEXT'),
				(u'invert_elev',	'REAL'),
				(u'div_link',		'TEXT'),
				# OVERFLOW, CUTOFF, TABULAR or WEIR
				(u'type',			'TEXT'),
				# below is mixted between types
				(u'd_curve',		'TEXT'),
				(u'q_min',			'REAL'),
				(u'height',			'REAL'),
				(u'discharge_coeff','REAL'),
				(u'max_depth',		'REAL'),
				(u'init_depth',		'REAL'),
				(u'surcharge_depth','REAL'),
				(u'ponded_area', 	'REAL')]
COLS_STORAGE =	[(u'cat',			'INTEGER PRIMARY KEY'),
				(u'name',			'TEXT'),
				(u'invert_elev',	'REAL'),
				(u'max_depth',		'REAL'),
				(u'init_depth',		'REAL'),
				(u'type',			'TEXT'), # TABULAR or FUNCTIONAL
				# follow is mixted between TAB and FUNC
				(u'curve',			'TEXT'),
				(u'coeff_a1',		'REAL'),
				(u'coeff_a2',		'REAL'),
				(u'coeff_a0',		'REAL'),
				(u'ponded_area', 	'REAL'),
				(u'fevap',			'REAL'),
				(u'suction_head',	'REAL'),
				(u'hyd_conduc',		'REAL'),
				(u'imd',			'REAL')]


COLS_CONDUIT = 	[(u'cat', 'INTEGER PRIMARY KEY'),
                 (u'name',			'TEXT'),
                 (u'in_node',		'TEXT'),
                 (u'out_node',		'TEXT'),
                 (u'length',			'REAL'),
                 (u'manning_n',		'REAL'),
                 (u'up_offset',		'REAL'),
                 (u'down_offset',	'REAL'),
                 (u'init_flow',		'REAL'),
                 (u'max_flow',		'REAL')] + XSECTION

COLS_PUMP =		[(u'cat',	'INTEGER PRIMARY KEY'),
                 (u'name',			'TEXT'),
                 (u'in_node',		'TEXT'),
                 (u'out_node',		'TEXT'),
                 (u'p_curve',		'TEXT'),
                 (u'status',		'TEXT'),
                 (u'startup',		'REAL'),
                 (u'shutoff',		'REAL')]

COLS_ORIFICE =	[(u'cat',	'INTEGER PRIMARY KEY'),
				(u'name',			'TEXT'),
				(u'in_node',		'TEXT'),
				(u'out_node',		'TEXT'),
				(u'type',			'TEXT'),
				(u'offset',			'REAL'),
				(u'discharge_coeff','REAL'),
				(u'flap',			'TEXT'),
				(u'orate',			'REAL')] + XSECTION

COLS_WEIR =		[(u'cat',			'INTEGER PRIMARY KEY'),
				(u'name',			'TEXT'),
				(u'in_node',		'TEXT'),
				(u'out_node',		'TEXT'),
				(u'type',			'TEXT'),
				(u'offset',			'REAL'),
				(u'discharge_coeff','REAL'),
				(u'flap',			'TEXT'),
				(u'end_contraction','REAL'),
				(u'discharg_coeff2','REAL')] + XSECTION

COLS_OUTLET =	[(u'cat',			'INTEGER PRIMARY KEY'),
				(u'name',			'TEXT'),
				(u'in_node',		'TEXT'),
				(u'out_node',		'TEXT'),
				(u'offset',			'REAL'),
                # TABULAR/DEPTH, TABULAR/HEAD, FUNCTIONAL/DEPTH, FUNCTIONAL/HEAD
				(u'type',			'TEXT'),
                # below is mixted between types
				(u'param1',			'TEXT'),
				(u'param2',			'TEXT'),
				(u'param3',			'TEXT')]

#~ COLS_LOSSE = 		[(u'conduit',		'TEXT'),
					#~ (u'kentry',			'REAL'),
					#~ (u'kexit',			'REAL'),
					#~ (u'kavg',			'REAL'),
					#~ (u'flap',			'TEXT')]
#~ COLS_TRANSECT =		[]
#~ COLS_CONTROL =		[]
#~ COLS_POLLUTANT =	[]
#~ COLS_LANDUSE =		[]
#~ COLS_COVERAGE =		[]
#~ COLS_BUILDUP =		[]
#~ COLS_WASHOFF =		[]
#~ COLS_TREATMENT =	[]
#~ COLS_DWF =			[]
#~ COLS_PATTERN =		[]
#~ COLS_INFLOW =		[]
#~ COLS_LOADING =		[]
#~ COLS_RDII =			[]
#~ COLS_HYDROGRAPH =	[]
#~ COLS_CURVE =		[]
#~ COLS_TIMESERIE =	[]
#~ COLS_LID_CONTROL = 	[]
#~ COLS_LID_USAGE =	[]
#~ COLS_TAG = 			None # Unknown use
#~ COLS_MAP = 			None # Not necesary
#~ COLS_COORDINATE = 	None # stored as geographic object
#~ COLS_VERTICE = 		None # stored as geographic object
#~ COLS_POLYGON = 		None # stored as geographic object
#~ COLS_LABEL = 		None # stored as geographic object
#~ COLS_BACKDROP = 	None # stored as geographic object
#~ COLS_SYMBOL = 		None # stored as geographic object
#~ COLS_PROFILE = 		None # Unknown use
#~ COLS_GWF = 			None # Unknown use
#~ COLS_ADJUST = 		None # Unknown use

COLS_NETWORK_ELEM = [(u'cat',  'INTEGER PRIMARY KEY'),
                     (u'name', 'TEXT'),
                     (u'type', 'TEXT')]


# define DB table suffix
TN_TITLE = '_title'
TN_OPTION = '_option'
TN_REPORT = '_report'
TN_FILES = '_files'
TN_RAINGAGE = '_raingage'
TN_EVAPORATION = '_evaporation'
TN_TEMPERATURE = '_temperature'
TN_SUBCATCH = '_subcatch'
TN_INFILTR = '_infiltr'
TN_AQUIFER = '_aquifer'
TN_GROUNDWAT = '_groundwat'
TN_SNOWPACK = '_snowpack'
TN_JUNCTION = '_junction'
TN_OUTFALL = '_outfall'
TN_DIVIDER = '_divider'
TN_STORAGE = '_storage'
TN_CONDUIT = '_conduit'
TN_PUMP = '_pump'
TN_ORIFICE =  '_orifice'
TN_WEIR = '_weir'
TN_OUTLET =  '_outlet'
TN_XSECTION =  '_xsection'
TN_LOSSE = '_losse'
TN_TRANSECT = '_transect'
TN_CONTROL = '_control'
TN_POLLUTANT = '_pollutant'
TN_LANDUSE = '_landuse'
TN_COVERAGE = '_coverage'
TN_BUILDUP = '_buildup'
TN_WASHOFF = '_washoff'
TN_TREATMENT = '_treatment'
TN_DWF = '_dwf'
TN_PATTERN = '_pattern'
TN_INFLOW = '_inflow'
TN_LOADING = '_loading'
TN_RDII = '_rdii'
TN_HYDROGRAPH = '_hydrograph'
TN_CURVE = '_curve'
TN_TIMESERIE = '_timeserie'
TN_LID_CONTROL = '_lid_control'
TN_LID_USAGE = '_lid_usage'
TN_GEN = ''


# define list of table suffix
TABLE_SUFFIX = [# TN_TITLE,
                #~ TN_OPTION,
                #~ TN_REPORT,
                #~ TN_FILES,
                #~ TN_RAINGAGE,
                #~ TN_EVAPORATION,
                #~ TN_TEMPERATURE,
                #~ TN_SUBCATCH,
                #~ TN_INFILTR,
                #~ TN_AQUIFER,
                #~ TN_GROUNDWAT,
                #~ TN_SNOWPACK,
                TN_JUNCTION,
                TN_OUTFALL,
                TN_DIVIDER,
                TN_STORAGE,
                TN_CONDUIT,
                TN_PUMP,
                TN_ORIFICE, 
                TN_WEIR,
                TN_OUTLET, 
                #~ TN_XSECTION, 
                #~ TN_LOSSE,
                #~ TN_TRANSECT,
                #~ TN_CONTROL,
                #~ TN_POLLUTANT, 
                #~ TN_LANDUSE,
                #~ TN_COVERAGE,
                #~ TN_BUILDUP,
                #~ TN_WASHOFF,
                #~ TN_TREATMENT,
                #~ TN_DWF,
                #~ TN_PATTERN,
                #~ TN_INFLOW,
                #~ TN_LOADING,
                #~ TN_RDII,
                #~ TN_HYDROGRAPH,
                #~ TN_CURVE,
                #~ TN_TIMESERIE,
                #~ TN_LID_CONTROL,
                #~ TN_LID_USAGE,
                TN_GEN]

# GRASS layer numbers
#~ LAYER_GENERAL = 1
LAYER_JUNCTION = 1
LAYER_OUTFALL =  2
LAYER_DIVIDER =  3
LAYER_STORAGE =  4
LAYER_CONDUIT = 5
LAYER_PUMP =    6
LAYER_ORIFICE = 7
LAYER_WEIR =    8
LAYER_OUTLET =  9


# define the caracteristics of elements related to geographic objects
# {name: [table_suffix, col_descr, layer_number], etc.}
LayerDescr = namedtuple('LayerDescr', ['table_suffix', 'cols', 'layer_number'])
K_LNK_ELEM = {KWD_SECT_JUNCTION: LayerDescr(TN_JUNCTION, COLS_JUNCTION, LAYER_JUNCTION),
              KWD_SECT_OUTFALL:  LayerDescr(TN_OUTFALL, COLS_OUTFALL, LAYER_OUTFALL),
              KWD_SECT_DIVIDER:  LayerDescr(TN_DIVIDER, COLS_DIVIDER, LAYER_DIVIDER),
              KWD_SECT_STORAGE:  LayerDescr(TN_STORAGE, COLS_STORAGE, LAYER_STORAGE),
              KWD_SECT_CONDUIT:  LayerDescr(TN_CONDUIT, COLS_CONDUIT, LAYER_CONDUIT),
              KWD_SECT_PUMP:     LayerDescr(TN_PUMP, COLS_PUMP, LAYER_PUMP),
              KWD_SECT_ORIFICE:  LayerDescr(TN_ORIFICE, COLS_ORIFICE, LAYER_ORIFICE),
              KWD_SECT_WEIR:     LayerDescr(TN_WEIR, COLS_WEIR, LAYER_WEIR),
              KWD_SECT_OUTLET:   LayerDescr(TN_OUTLET, COLS_OUTLET, LAYER_OUTLET),
              }

# node sub-types
W_JUNCTION = KWD_SECT_JUNCTION
W_OUTFALL =  KWD_SECT_OUTFALL
W_STORAGE =  KWD_SECT_STORAGE
W_DIVIDER =  KWD_SECT_DIVIDER

KWD_TYPE_NODE = [W_JUNCTION,
                 W_OUTFALL,
                 W_STORAGE,
                 W_DIVIDER]

# link sub-types
W_CONDUIT = KWD_SECT_CONDUIT
W_PUMP =    KWD_SECT_PUMP
W_ORIFICE = KWD_SECT_ORIFICE
W_WEIR =    KWD_SECT_WEIR
W_OUTLET =  KWD_SECT_OUTLET

KWD_TYPE_LINK = [W_CONDUIT,
                 W_PUMP,
                 W_ORIFICE,
                 W_WEIR,
                 W_OUTLET]

OUTFALL_TYPES = ['FREE',
                 'NORMAL',
                 'FIXED',
                 'TIDAL',
                 'TIMESERIES']

DIVIDER_TYPES = ['OVERFLOW',
                 'CUTOFF',
                 'TABULAR',
                 'WEIR']

STORAGE_TYPES = ['TABULAR',
                 'FUNCTIONAL']
