#!/usr/bin/env python
# -*- coding: UTF-8 -*

############################################################################
#
# MODULE:       v.in.swmm
# AUTHOR(S):    Laurent Courty
# PURPOSE:      Import EPA SWMM5 INP file as vector map
# COPYRIGHT:    (C) 2014-2016 by Laurent Courty
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file COPYING that comes with GRASS
#               for details.
#
#############################################################################

#%module
#% description: Import EPA SWMM5 INP file as vector map.
#% keywords: vector
#% keywords: SWMM
#% keywords: import
#%end

#%option G_OPT_F_INPUT
#% description: Input INP file
#% required : yes
#%end

#%option G_OPT_V_OUTPUT
#% description: Created vector map
#% required : yes
#%end


import sys
import networkx as nx
from collections import namedtuple

# Import variables defining the handling of swmm inp file
from inp_kwd import *

from grass.pygrass.vector.geometry import Point, Line
import grass.script as gscript
from grass.pygrass.vector import VectorTopo
from grass.pygrass.vector.basic import Cats
from grass.pygrass.vector.table import Link
from grass.pygrass.messages import Messenger

from inp_parser import SwmmInputParser

#############

msgr = Messenger()
LinkDescr = namedtuple('LinkDescr', ['layer', 'table'])


def create_db_links(vect_map):
    """vect_map an open vector map
    """
    dblinks = {}
    for layer_name, layer_dscr in K_LNK_ELEM.iteritems():
        # Create DB links
        dblink = Link(layer=layer_dscr.layer_number, name=layer_name,
                      table=vect_map.name + layer_dscr.table_suffix, key='cat')
        # add link to vector map
        if dblink not in vect_map.dblinks:
            vect_map.dblinks.add(dblink)
        # create table
        dbtable = dblink.table()
        dbtable.create(layer_dscr.cols, overwrite=True)
        dblinks[layer_name] = LinkDescr(dblink.layer, dbtable)
    return dblinks

def write_vector(map_name, drainage_network):
    """drainage_network is a networkx object
    """
    node_cat = {}
    link_cat = {}
    with VectorTopo(map_name, mode='w') as vect_map:
        # create db links and tables
        dblinks = create_db_links(vect_map)

        # set category manually
        cat_num = 1

        # dict to keep DB infos to write DB after geometries
        db_info = {k:[] for k in K_LNK_ELEM}

        # Points
        msgr.verbose("points: {}".format(nx.number_of_nodes(drainage_network)))
        for node_name, node_attr in drainage_network.nodes(data=True):
            point = Point(*node_attr['coordinates'])
            # add values
            map_layer, dbtable = dblinks[node_attr['type']]
            write_geometry(vect_map, point, cat_num, map_layer)
            # keep DB info
            attrs = tuple([cat_num] + [i for i in node_attr['values']])
            db_info[node_attr['type']].append(attrs)
            # bump cat
            cat_num += 1

        # Lines
        msgr.verbose("points: {}".format(nx.number_of_edges(drainage_network)))
        # iterate edges
        for in_node, out_node, edge_data in drainage_network.edges_iter(data=True):
            # assemble geometry
            in_node_coor = drainage_network.node[in_node]['coordinates']
            out_node_coor = drainage_network.node[out_node]['coordinates']
            line_object = Line([in_node_coor]
                               + edge_data['vertices']
                               + [out_node_coor])
            # set category and layer link
            map_layer, dbtable = dblinks[edge_data['type']]
            write_geometry(vect_map, line_object, cat_num, map_layer)
            # keep DB info
            attrs = tuple([cat_num] + [i for i in edge_data['values']])
            db_info[edge_data['type']].append(attrs)
            # bump cat
            cat_num += 1

    # write DB
    write_db(dblinks, db_info)

def write_db(dblinks, db_info):
    for geom_type, attrs in db_info.iteritems():
        map_layer, dbtable = dblinks[geom_type]
        for attr in attrs:
            dbtable.insert(attr)
        dbtable.conn.commit()

def write_geometry(vector_map, geom, cat_num, map_layer):
    """Write geometry in the adequate layer
    """
    cats = Cats(geom.c_cats)
    cats.reset()
    cats.set(cat_num, map_layer)
    # write geometry
    vector_map.write(geom)


def create_network(nodes, links):
    """create a networkx object using given links and nodes dicts
    """
    # new directed multigraph
    network = nx.MultiDiGraph()
    for node_name, node_value in nodes.iteritems():
        network.add_node(node_name, type=node_value.type,
                         coordinates=node_value.coordinates,
                         values=node_value.values)
    for link_name, link_value in links.iteritems():
        network.add_edge(link_value.in_node, link_value.out_node, name=link_name,
                         vertices=link_value.vertices, type=link_value.type,
                         values=link_value.values)
    return network


def main():
    inp_file = options['input']  # a swmm inp text file
    output_map = options['output']  # name of a new GRASS vector map

    msgr.message("Open and read the input file...")
    inp_parser = SwmmInputParser(inp_file)
    nodes = inp_parser.get_nodes()
    links = inp_parser.get_links()

    msgr.message("Creating network object...")
    drainage_network = create_network(nodes, links)

    msgr.message("Write vector...")
    write_vector(output_map, drainage_network)

    return 0


if __name__ == "__main__":
    options, flags = gscript.parser()
    sys.exit(main())
