# -*- coding: UTF-8 -*

# read and parse input swmm imp text file

import sys
import collections

# Import variables defining the handling of swmm inp file
from inp_kwd import *
from network_objects import SwmmNode, Junction, Outfall, Divider, Storage
from network_objects import SwmmLink, XSection, Conduit, Pump, Orifice, Weir, Outlet

Coordinates = collections.namedtuple('Coordinates', ['x', 'y'])


class SwmmInputParser(object):
    """A parser for swmm input text file
    """
    def __init__(self, input_file):
        # read and parse the input file
        self.inp = dict.fromkeys(KWD_SECTS)
        self.read_inp(input_file)

    def section_kwd(self, sect_name):
        """verify if the given section name is a valid one.
        Return the corresponding section keyword, None if unknown
        """
        # check done in lowercase, without final 's'
        section_valid = sect_name.lower().rstrip('s')
        result = None
        for kwd in KWD_SECTS:
            if kwd.startswith(section_valid):
                result = kwd
        return result

    def read_inp(self, input_file):
        """Read the inp file and generate a dictionary of lists
        """
        with open(input_file, 'r') as inp:
            for line in inp:
                # got directly to next line if comment or empty
                if line.startswith(';') or not line.strip():
                    continue
                # retrive current standard section name
                elif line.startswith('['):
                    current_section = self.section_kwd(line.strip().strip('[] '))
                    if current_section is None:
                        print("Unknown section: {}".format(line))
                else:
                    # if it's the first line of the section, create an empty list
                    if self.inp[current_section] is None:
                        self.inp[current_section] = []
                    # add value to list
                    self.inp[current_section].append(line.strip().split())
        return self

    def get_junction(self, junction_name):
        """return a namedtuple
        """
        junction_values = []
        for j in self.inp[KWD_SECT_JUNCTION]:
            if junction_name == j[0]:
                junction_values.append(j[0])
                junction_values.extend(j[1:])
                return Junction._make(junction_values)

    def get_outfall(self, outfall_name):
        """return a namedtuple
        """
        values = []
        for o in self.inp[KWD_SECT_OUTFALL]:
            if outfall_name == o[0]:
                values.append(o[0])
                values.append(float(o[1]))
                values.extend(o[2:])
                # pad list to match tuple length
                values += [None] * (len(Outfall._fields) - len(values))
                return Outfall._make(values)

    def get_divider(self, divider_name):
        """return a namedtuple
        """
        values = []
        for d in self.inp[KWD_SECT_DIVIDER]:
            if outfall_name == d[0]:
                values.append(d[0])
                values.append(float(d[1]))
                values.extend(d[2:5])
                values.extend([float(i) for i in d[5:]])
                # pad list to match tuple length
                values += [None] * (len(Divider._fields) - len(values))
                return Divider._make(values)

    def get_storage(self, storage_name):
        """return a namedtuple
        """
        values = []
        for s in self.inp[KWD_SECT_STORAGE]:
            if storage_name == s[0]:
                values.append(s[0])
                values.extend([float(i) for i in s[1:4]])
                values.extend(s[4:6])
                values.extend([float(i) for i in s[6:]])
                # pad list to match tuple length
                values += [None] * (len(Storage._fields) - len(values))
                return Storage._make(values)

    def get_conduit(self, conduit_name):
        """return Conduit namedtuple
        """
        values = []
        for c in self.inp[KWD_SECT_CONDUIT]:
            if conduit_name == c[0]:
                for i in c:
                    # if it cannot be translated to float, it is text
                    try:
                        values.append(float(i))
                    except ValueError:
                        values.append(i)
                # add cross-section
                values.extend(self.get_xsection(conduit_name))
                return Conduit._make(values)

    def get_pump(self, pump_name):
        """return Conduit namedtuple
        """
        values = []
        for c in self.inp[KWD_SECT_PUMP]:
            if pump_name == c[0]:
                for i in c:
                    # if it cannot be translated to float, it is text
                    try:
                        values.append(float(i))
                    except ValueError:
                        values.append(i)
                # pad to fit tuple size
                values += [None] * (len(Pump._fields) - len(values))
                return Pump._make(values)

    def get_orifice(self, orifice_name):
        """return Conduit namedtuple
        """
        values = []
        for c in self.inp[KWD_SECT_ORIFICE]:
            if orifice_name == c[0]:
                for i in c:
                    # if it cannot be translated to float, it is text
                    try:
                        values.append(float(i))
                    except ValueError:
                        values.append(i)
                # pad to fit tuple size
                values += [None] * (len(Orifice._fields) - len(values))
                # add cross-section
                values.extend(self.get_xsection(orifice_name))
                return Orifice._make(values)

    def get_weir(self, weir_name):
        """return Conduit namedtuple
        """
        values = []
        for c in self.inp[KWD_SECT_WEIR]:
            if weir_name == c[0]:
                for i in c:
                    # if it cannot be translated to float, it is text
                    try:
                        values.append(float(i))
                    except ValueError:
                        values.append(i)
                # pad to fit tuple size
                values += [None] * (len(Weir._fields) - len(values))
                # add cross-section
                values.extend(self.get_xsection(weir_name))
                return Weir._make(values)

    def get_outlet(self, outlet_name):
        """return Conduit namedtuple
        """
        values = []
        for c in self.inp[KWD_SECT_OUTLET]:
            if outlet_name == c[0]:
                for i in c:
                    # if it cannot be translated to float, it is text
                    try:
                        values.append(float(i))
                    except ValueError:
                        values.append(i)
                # pad to fit tuple size
                values += [None] * (len(Outlet._fields) - len(values))
                return Outlet._make(values)

    def get_xsection(self, link_name):
        """return
        """
        xs_values = []
        for xs in self.inp[KWD_SECT_XSECTION]:
            if link_name == xs[0]:
                # shape is txt
                xs_values.append(xs[1])
                # geom are floats
                xs_values.extend([float(i) for i in xs[2:5]])
                # barrels and culverts are int
                xs_values.extend([int(i) for i in xs[5:]])
                # pad list to match length
                xs_values += [None] * (len(XSection._fields) - len(xs_values))
                return XSection._make(xs_values)

    def get_nodes_coordinates(self):
        """return a dict of namedtuples
        """
        d = {}
        for c in self.inp[KWD_SECT_COORDINATE]:
            name = c[0]
            values = [float(v) for v in c[1:]]
            d[name] = Coordinates._make(values)
        return d

    def get_nodes(self):
        """Return a dict of namedtuple
        ('name', 'type' 'invert_elevation', 'coordinates')
        """
        nodes_dict = {}
        nodes_coor = self.get_nodes_coordinates()
        # list all node type
        for k in KWD_TYPE_NODE:
            # a list of lists
            nodes = self.inp[k]
            if nodes is not None:
                for node in nodes:
                    # store in dict
                    ID = node[0]
                    if k == KWD_SECT_JUNCTION:
                        values = self.get_junction(ID)
                    elif k == KWD_SECT_OUTFALL:
                        values = self.get_outfall(ID)
                    elif k == KWD_SECT_DIVIDER:
                        values = self.get_divider(ID)
                    elif k == KWD_SECT_STORAGE:
                        values = self.get_storage(ID)
                    nodes_dict[ID] = SwmmNode(type=k,
                                          coordinates=nodes_coor[ID],
                                          values=values)
        return nodes_dict

    def get_links(self):
        """Return a dict of namedtuple
        ('name', 'type', 'vertices', 'in_node', 'out_node')
        """
        links_dict = {}
        # loop thru all types of links
        for k in KWD_TYPE_LINK:
            links = self.inp[k] # list of lists
            if links is not None:
                for ln in links:
                    ID = ln[0]
                    # names of link, inlet and outlet nodes
                    values = None
                    if k == KWD_SECT_CONDUIT:
                        values = self.get_conduit(ID)
                    elif KWD_SECT_PUMP:
                        values = self.get_pump(ID)
                    elif k == KWD_SECT_ORIFICE:
                        values = self.get_orifice(ID)
                    elif k == KWD_SECT_WEIR:
                        values = self.get_weir(ID)
                    elif k == KWD_SECT_OUTLET:
                        values = self.get_outlet(ID)
                    links_dict[ID] = SwmmLink(type=k,
                                          vertices=self.get_vertices(ID),
                                          in_node=ln[1], out_node=ln[2],
                                          values=values)
        return links_dict

    def get_vertices(self, link_name):
        """For a given link name, return a list of Coordinates objects
        """
        vertices = []
        for vertex in self.inp[KWD_SECT_VERTICE]:
            if link_name == vertex[0]:
                vertex_c = Coordinates(float(vertex[1]), float(vertex[2]))
                vertices.append(vertex_c)
        return vertices
